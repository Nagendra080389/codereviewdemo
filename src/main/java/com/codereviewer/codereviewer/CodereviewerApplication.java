package com.codereviewer.codereviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CodereviewerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodereviewerApplication.class, args);
	}
}
